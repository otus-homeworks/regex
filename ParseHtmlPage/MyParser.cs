﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace UploadPicturesFromHtmlPage
{
    public class MyParser
    {
        // Для извлечения из ссылки имени файла (строка ищется с конца).
        private static readonly Regex _rgxImgName = new Regex(@"\/(?<name>[^\/.]+?\.\w+$)", RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);

        // Поиск фрагментов вида <img src = "photo.jpg' alt="photo" в строке data.
        public static List<string> SelectImageUrls(string data)
        {
            var rgxGetImgUrls = new Regex(@"<img.+src\s*=\s*[""'](?<p_url>[^""']+[\.jpg|\.png])\s*[""']", RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);

            MatchCollection matches = rgxGetImgUrls.Matches(data);
            return matches
                .Cast<Match>()
                .Select(m => m.Groups["p_url"].Value)
                .Distinct()
                .ToList();
        }

        /// <summary>
        /// Из ссылки на картинку извлекается имя файла. Если
        /// </summary>
        /// <param name="url">Ссылкак на картинку из которой следует извлечь имя файла.</param>
        /// <param name="name">Имя файла, выделенное из передаваемой в url ссылки.</param>
        /// <returns>Возвращает true, если удалось выделить имя файла и flase в противном случае.</returns>
        public static bool SelectFileNameFromUrl(string url, out string name)
        {
            Match match = _rgxImgName.Match(url);

            if (match.Success && url.Contains("http"))
            {
                name = match.Groups["name"].Value;
                return true;
            }
            else
            {
                name = "";
                return false;
            }
        }
    }
}
