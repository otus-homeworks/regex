﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace UploadPicturesFromHtmlPage
{
    public static class ImagesLoader
    {
        public static void UploadImages(List<string> imgUrls)
        {
            if (imgUrls.Count() > 0)
            {
                string targetPath = Path.Combine(Directory.GetCurrentDirectory(), "DownLoadedImages");
                if (!Directory.Exists(targetPath))
                    Directory.CreateDirectory(targetPath);

                var downloadTasks = new List<Task>();

                foreach (string s in imgUrls)
                {
                    if (MyParser.SelectFileNameFromUrl(s, out string fileName))
                    {
                        Console.WriteLine($"Начал скачивать {fileName} по: {s}");
                        using WebClient client = new WebClient();   // Для каждой Task нужен свой отдельный экземпляр.
                        var taskDownload = client.DownloadFileTaskAsync(s, targetPath + "\\" + fileName);
                        downloadTasks.Add(taskDownload);
                    }
                    else
                        Console.WriteLine($"В строке {s} не удалось выделить имя файла или адрес картинки.");
                }

                try
                {
                    Task.WaitAll(downloadTasks.ToArray());
                }
                catch (AggregateException ae)
                {
                    Console.WriteLine("\nСледующие исключения были вызваны при вызове WaitAll()");
                    for (int j = 0; j < ae.InnerExceptions.Count; j++)
                    {
                        Console.WriteLine(ae.InnerExceptions[j]);
                    }
                }
            }
        }
    }
}
