﻿using System;
using System.Linq;
using System.Net;

namespace UploadPicturesFromHtmlPage
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string urlDefault = "https://habr.com/ru/company/microsoft/blog/134160/";
                Console.Write("Введите url страницы. Пустая строка соответствует url по умолчанию\n>:");
                string url = Console.ReadLine();
                if (url == string.Empty)
                    url = urlDefault;

                using WebClient client = new WebClient();
                var data = client.DownloadString(url);
                Console.WriteLine($"Страница успешно загружена ({data.Length} символов)...");

                var imgUrls = MyParser.SelectImageUrls(data);
                Console.WriteLine($"Найдено {imgUrls.Count()} адресов изображений...");

                ImagesLoader.UploadImages(imgUrls);
                Console.WriteLine("\nЗагрузка файлов завершена.");
            }
            catch (WebException webEx)
            {
                Console.WriteLine(webEx.ToString());
                if (webEx.Status == WebExceptionStatus.ConnectFailure)
                {
                    Console.WriteLine("Are you behind a firewall?  If so, go through the proxy server. Or any another problems.");
                }
            }

            Console.ReadLine();
        }
    }
}


