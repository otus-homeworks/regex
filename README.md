Домашнее задание

Найти все картинки на HTML-странице
Цель:

Составление регулярного выражения для парсинга HTML-страницы.

Составить регулярное выражение для поиска адресов всех изображений в HTML тексте. Написать программу, которая:

    На вход принимает URL-адрес,
    Загружает HTML-текст с этого адреса.
    Находит URL-адреса всех изображений, используя регулярное выражение. <img src="photo.jpg' alt="photo" /> 4*. 
    Сохраняется все картинки в отдельные файлы.

В отчёте написать:

    Составленное регулярное выражение.
    Ссылку на репозиторий с вашим проектом.